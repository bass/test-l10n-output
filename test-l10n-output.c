#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>
#include <errno.h>

#include <locale.h>
#include <langinfo.h>

#include <iconv.h>

#if defined(__sun) && defined(__SVR4)
/*
 * In Solaris, sizeof(wchar_t) is normally 4.
 *
 * Both UCS-2 and UCS-4 can be converted to UTF-8,
 * while UTF-16 can only be converted to UCS-4 (see iconv_unicode(5)),
 * so UCS-2 is preferred to UTF-16.
 *
 * "WCHAR_T" is a sane default which will be rejected by Solaris' iconv_open(),
 * but still is a valid input for iconv_open() from GNU libiconv.
 */
#define ICONV_WCHAR_T sizeof(wchar_t) == 4 ? "UCS-4" : sizeof(wchar_t) == 2 ? "UCS-2" : "WCHAR_T"
#else
#define ICONV_WCHAR_T "WCHAR_T"
#endif

#if defined(__sun) && defined(__SVR4)
#define PRINTF_SIZE_T "%u"
#else
#define PRINTF_SIZE_T "%zu"
#endif

char *iconvToNewA(const char * const source, const char *sourceCharset, const char *targetCharset);

char *iconvToNewW(const wchar_t * const source, const char *targetCharset);

/*
 * UTF-8 may use up to 6 bytes to encode a char.
 * For Cyrillic, 2 bytes is enough though.
 */
const unsigned int bytesPerChar = 2;

int main() {
	const char *locale = setlocale(LC_ALL, "");
	if (locale == NULL) {
		fprintf(stderr, "Locale unavailable.\n");
	}

	const char *charset = nl_langinfo(CODESET);
	printf("locale: %s\n", locale);
	printf("run-time charset: %s\n", charset);
	const wchar_t * const wcs = L"\u0410\u0411\u0412\u0413\u0414\u0415\u0401\u0416\u0417\u0418\u0419\u041a\u041b\u041c\u041d\u041e\u041f\u0420\u0421\u0422\u0423\u0424\u0425\u0426\u0427\u0428\u0429\u042a\u042b\u042c\u042d\u042e\u042f\u0430\u0431\u0432\u0433\u0434\u0435\u0451\u0436\u0437\u0438\u0439\u043a\u043b\u043c\u043d\u043e\u043f\u0440\u0441\u0442\u0443\u0444\u0445\u0446\u0447\u0448\u0449\u044a\u044b\u044c\u044d\u044e\u044f";

	char * const mbs = iconvToNewW(wcs, charset);
	if (mbs != NULL) {
		printf("wcs->mbs from charset %s to local charset %s using iconv(), length " PRINTF_SIZE_T " char(s): %s\n", ICONV_WCHAR_T, charset, wcslen(wcs), mbs);
	}
	free(mbs);

	return 0;
}

char *iconvToNewA(const char * const source, const char *sourceCharset, const char *targetCharset) {
	const int wideChar = !strcmp(sourceCharset, ICONV_WCHAR_T);

	/*
	 * bytesPerChar only makes sense when converting to UTF-8.
	 */
	const size_t sourceLength = wideChar ? wcslen((wchar_t *) source) : strlen(source); // w/o trailing  \0
	const size_t targetLength = sourceLength * (wideChar || strcmp(sourceCharset, targetCharset)
			? bytesPerChar
			: 1); // w/o trailing \0

	char * const target = malloc(targetLength + 1);

#if defined(__sun) && defined(__SVR4)
	/*
	 * On Solaris, iconv_open() fails if the source and target charsets are
	 * the same, so using memcpy() instead.
	 */
	if (!wideChar && !strcmp(sourceCharset, targetCharset)) {
		/*
		 * Conversion to the same charset.
		 */
		return memcpy(target, source, sourceLength);
	}
#endif

	errno = 0;
	const iconv_t descriptor = iconv_open(targetCharset, sourceCharset);
	if (descriptor == (iconv_t) -1) {
#if defined(__sun) && defined(__SVR4)
		/*
		 * Solaris can't convert from a single-byte charset to another
		 * single-byte charset, so UTF-8 must be used for intermediate
		 * conversion.
		 */
		if (errno == EINVAL && strcmp(targetCharset, "UTF-8")) {
			char * const utf8String = iconvToNewA(source, sourceCharset, "UTF-8");
			char * const s = iconvToNewA(utf8String, "UTF-8", targetCharset);
			free(utf8String);

			return s;
		}
#endif

		fprintf(stderr, "iconv_open(\"%s\", \"%s\") failed: errno = %d\n", targetCharset, sourceCharset, errno);
		perror("iconv_open() failed");
		return NULL;
	}

#if defined(__sun) && defined(__SVR4)
	const char *inBuf = source;
#else
	char *inBuf = (char *) source;
#endif
	char *outBuf = target;
	size_t inBytesLeft = (sourceLength + 1) * (wideChar ? sizeof(wchar_t) : sizeof(char));
	size_t outBytesLeft = targetLength + 1;

	errno = 0;
	if (iconv(descriptor, &inBuf, &inBytesLeft, &outBuf, &outBytesLeft) == (size_t) -1) {
		fprintf(stderr, "iconv() failed: errno = %d\n", errno);
		perror("iconv() failed");
		return NULL;
	}

	errno = 0;
	if (iconv_close(descriptor) == -1) {
		fprintf(stderr, "iconv_close() failed: errno = %d\n", errno);
		perror("iconv_close() failed");
		return NULL;
	}

	return target;
}

char *iconvToNewW(const wchar_t * const source, const char *targetCharset) {
	return iconvToNewA((char *) source, ICONV_WCHAR_T, targetCharset);
}
