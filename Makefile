CFLAGS = -std=c99 -O0 -g3 -pedantic -Wall -Wextra -Wconversion

ifdef COMSPEC
EXESUFFIX = .exe
LDLIBS = -liconv
endif

.PHONY: all
all: test-l10n-output$(EXESUFFIX)

test-l10n-output$(EXESUFFIX): test-l10n-output.o
	$(CC) -o $@ $< $(LDLIBS)

.PHONY: clean
clean:
	$(RM) test-l10n-output$(EXESUFFIX) test-l10n-output.o
